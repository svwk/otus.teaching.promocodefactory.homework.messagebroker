using System.Threading.Tasks;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;

namespace Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions
{
    public interface IGivingToCustomerService
    {
        public Task<bool> GivePromoCodesToCustomersWithPreferenceAsync(PromoCode promocode);
    }
}