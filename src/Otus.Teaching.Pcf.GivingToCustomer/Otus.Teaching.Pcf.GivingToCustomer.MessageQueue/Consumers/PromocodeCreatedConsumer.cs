using System.Threading.Tasks;
using MassTransit;
using MassTransit.Definition;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.GivingToCustomer.MessageQueue.Contracts;
using Otus.Teaching.Pcf.MessageQueue.Contracts;

namespace Otus.Teaching.Pcf.GivingToCustomer.MessageQueue.Consumers
{
    public class PromocodeCreatedConsumer : IConsumer<IPromoCodeCreated>
    {
        private readonly IGivingToCustomerService _givingToCustomerService;

        public PromocodeCreatedConsumer(IGivingToCustomerService administrationService)
        {
            _givingToCustomerService = administrationService;
        }

        public async Task Consume(ConsumeContext<IPromoCodeCreated> context)
        {
            var promocode = new PromoCode()
            {
                ServiceInfo = context.Message.ServiceInfo,
                PartnerId = context.Message.PartnerId,
                Id = context.Message.PromoCodeId,
                Code = context.Message.Code,
                PreferenceId = context.Message.PreferenceId,
                BeginDate = context.Message.BeginDate,
                EndDate = context.Message.EndDate
            };
            await _givingToCustomerService.GivePromoCodesToCustomersWithPreferenceAsync(promocode);
        }
    }

    public class PromocodeCreatedConsumerDefinition : ConsumerDefinition<PromocodeCreatedConsumer>
    {
        public PromocodeCreatedConsumerDefinition()
        {
            EndpointName = QueueNames.PromocodeCreatedQueue;
        }
    }
}