namespace Otus.Teaching.Pcf.GivingToCustomer.MessageQueue.Contracts
{
    public static class QueueNames
    {
        public static string PromocodeCreatedQueue = "PromocodeCreated_GivingToCustomer";
    }
}