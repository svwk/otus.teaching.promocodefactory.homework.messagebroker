using System;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Administration.Core.Abstractions
{
    public interface IAdministrationService
    {
        public Task<bool> UpdateAppliedPromocodesAsync(Guid id);
    }
}