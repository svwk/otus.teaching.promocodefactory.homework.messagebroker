namespace Otus.Teaching.Pcf.Administration.MessageQueue.Contracts
{
    public static class QueueNames
    {
        public static string PromocodeCreatedQueue = "PromocodeCreated_Administration";
    }
}