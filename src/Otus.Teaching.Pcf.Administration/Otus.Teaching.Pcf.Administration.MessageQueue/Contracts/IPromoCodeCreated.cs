using System;

namespace Otus.Teaching.Pcf.MessageQueue.Contracts
{
    public interface IPromoCodeCreated
    {
        Guid PromoCodeId { get; set; }

        string Code { get; set; }

        string ServiceInfo { get; set; }

        DateTime BeginDate { get; set; }

        DateTime EndDate { get; set; }

        Guid PartnerId { get; set; }

        Guid PreferenceId { get; set; }

        Guid? PartnerManagerId { get; set; }
    }
}