﻿using System;
using MassTransit;
using MassTransit.Definition;
using Microsoft.Extensions.DependencyInjection;
using Otus.Teaching.Pcf.Administration.MessageQueue.Consumers;
using Otus.Teaching.Pcf.Administration.MessageQueue.Options;

namespace Otus.Teaching.Pcf.Administration.MessageQueue
{
    public static class ServiceExtensions
    {
        public static IServiceCollection AddMassTransitService(this IServiceCollection services,
            MassTransitOptions settings)
        {
            if (settings?.Host == null || settings.VirtualHost == null || settings.UserName == null ||
                settings.Password == null)
            {
                throw new ArgumentNullException(
                    "Section 'mass-transit' configuration settings are not found in appSettings.json");
            }

            services.AddMassTransit(x =>
            {
                x.AddBus(busFactory =>
                {
                    var bus = Bus.Factory.CreateUsingRabbitMq(
                        cfg =>
                        {
                            cfg.Host(
                                settings.Host,
                                settings.Port,
                                settings.VirtualHost,
                                conf =>
                                {
                                    conf.Username(settings.UserName);
                                    conf.Password(settings.Password);
                                });
                            cfg.ConfigureEndpoints(busFactory, KebabCaseEndpointNameFormatter.Instance);
                            cfg.UseJsonSerializer();
                        });
                    return bus;
                });
                x.AddConsumer<PromocodeCreatedConsumer, PromocodeCreatedConsumerDefinition>();
            });
            services.AddMassTransitHostedService();
            return services;
        }
    }
}