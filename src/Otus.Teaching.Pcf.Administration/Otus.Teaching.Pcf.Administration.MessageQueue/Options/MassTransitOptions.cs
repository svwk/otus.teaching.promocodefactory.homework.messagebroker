namespace Otus.Teaching.Pcf.Administration.MessageQueue.Options
{
    public class MassTransitOptions
    {
        public string Host { get; set; }

        public ushort Port { get; set; }

        public string VirtualHost { get; set; }

        public string UserName { get; set; }

        public string Password { get; set; }
    }
}