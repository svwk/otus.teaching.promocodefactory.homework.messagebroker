using System.Threading.Tasks;
using MassTransit;
using MassTransit.Definition;
using Otus.Teaching.Pcf.Administration.Core.Abstractions;
using Otus.Teaching.Pcf.Administration.MessageQueue.Contracts;
using Otus.Teaching.Pcf.MessageQueue.Contracts;

namespace Otus.Teaching.Pcf.Administration.MessageQueue.Consumers
{
    public class PromocodeCreatedConsumer : IConsumer<IPromoCodeCreated>
    {
        private readonly IAdministrationService _administrationService;

        public PromocodeCreatedConsumer(IAdministrationService administrationService)
        {
            _administrationService = administrationService;
        }

        public async Task Consume(ConsumeContext<IPromoCodeCreated> context)
        {
            if (context.Message.PartnerManagerId != null)
                await _administrationService.UpdateAppliedPromocodesAsync(context.Message.PartnerManagerId.Value);
        }
    }

    public class PromocodeCreatedConsumerDefinition : ConsumerDefinition<PromocodeCreatedConsumer>
    {
        public PromocodeCreatedConsumerDefinition()
        {
            EndpointName = QueueNames.PromocodeCreatedQueue;
        }
    }
}