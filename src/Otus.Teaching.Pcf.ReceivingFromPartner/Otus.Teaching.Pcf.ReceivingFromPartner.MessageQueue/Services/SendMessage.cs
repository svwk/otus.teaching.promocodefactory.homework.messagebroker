using System.Threading.Tasks;
using MassTransit;
using Otus.Teaching.Pcf.MessageQueue.Contracts;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Abstractions.Gateways;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Domain;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.MessageQueue.Services
{
    public class SendMessage : IMessageQueueGateway
    {
        private readonly IPublishEndpoint _publishEndpoint;

        public SendMessage(IPublishEndpoint publishEndpoint)
        {
            _publishEndpoint = publishEndpoint;
        }

        public async Task GivePromoCodeToCustomer(PromoCode promoCode)
        {
            await _publishEndpoint.Publish<IPromoCodeCreated>(new
            {
                PromoCodeId = promoCode.Id,
                Code = promoCode.Code,
                ServiceInfo = promoCode.ServiceInfo,
                BeginDate = promoCode.BeginDate.ToShortDateString(),
                EndDate = promoCode.EndDate.ToShortDateString(),
                PartnerId = promoCode.Partner.Id,
                PreferenceId = promoCode.PreferenceId,
                PartnerManagerId = promoCode.PartnerManagerId
            });
        }
    }
}